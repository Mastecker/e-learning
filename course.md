# „E-Learning“
## ... ein neues Qualitätsmerkmal der Lehre?

### Was bedeutet E-Learning?
Das Lernen mit digitalen Medien hat sich in den letzten Jahren rasant entwickelt und wird in naher Zukunft auch weiterhin von neuen Erkenntnissen geprägt werden. Die Verwendung von digitalen Medien im Alltag ist zur Selbstverständlichkeit geworden, sodass diese beim Lehren und Lernen miteingesetzt werden. Dabei geht es um die Gestaltung von Lernangeboten mit digitalen Medien, für die es verschiedene Szenarien gibt. Bei den Szenarien handelt es sich meist um selbstgesteuertes Lernen, das alleine, mit einem Partner, in der Gruppe und/oder mit einer betreuenden Instanz stattfindet.
Die Nutzung digitaler Medien für Lehr- und Lernzwecke wird auch als E-Learning bezeichnet und schließt verschiedenste Gerätetypen (PC, Laptop, Tablet, Smartphone, Beamer, Interaktive Whiteboard, Technik zur Aufnahme und Wiedergabe von Medien) mit ein. Für den Begriff E-Learning lassen sich unterschiedliche Definitionen und Synonyme finden. Auf Grund des raschen Wandels der technischen und didaktischen Bedingungen, sind Definitionen wie diese schneller obsolet als sie erstellt werden können. Diese starke Dynamik im Bereich E-Learning entsteht durch die sich ständig verbesserten Möglichkeiten. E-Learning weist eine große Breite auf, sodass es keine allgemeingültige Definition gibt. Michael Kerres (2013) stellt eine weitgefasste Definition auf, die E-Learning als einen „Oberbegriff für alle Varianten der Nutzung digitaler Medien zu Lehr- und Lernzwecken, sei es auf digitalen Datenträgern oder über das Internet, etwa um Wissen zu vermitteln, für den zwischenmenschlichen Austausch oder das gemeinsame Arbeiten an digitalen Artefakten“ (S. 6) beschreibt und eine grundsätzliche Vorstellung geben soll, was mit E-Learning gemeint ist. In der Praxis lassen sich verschiedene Szenarien von E-Learning finden, die im Folgenden kurz dargestellt werden.

### E-Learning Szenarien
E-Learning in Lehr- und Lernarrangements kann auf verschiedenen Techniken und Szenarien beruhen. Am häufigsten finden die in der Tabelle dargestellten Szenarien ihre Anwendung in der Praxis. **(Tabelle noch fehlend)**

*Lernprogramme*

Bei Lernprogrammen handelt es sich um bestimmte Computeranwendungen mit denen Lerninhalte vermittelt werden. Dabei wird in Computer Based Training (CBT) und Web Based Training (WBT) unterschieden. Die beiden Formen unterscheiben sich darin, dass für das Lernen bei CBT lediglich ein Computer benötigt wird und beim WBT zusätzlich ein Internetanschluss vorhanden sein muss. Diese Lernprogramme finden besonders im Alltag statt, wenn z.B. für eine theoretische Fahrprüfung gelernt werden muss oder eine neue Sprache mit Lernpro-grammen erlernt wird. Charakteristisch für dieses Lernszenario ist, dass die Lernenden selbstständig festlegen, wann, wo und wie lange gelernt wird, die Inhalte und das Lerntempo wird selbst festgelegt. Zudem können die Inhalte beliebig oft wiederholt und geübt werden.
Der Vorteil des WBTs ist, dass die Anwendungen laufend aktualisiert und verbessert werden können. Dafür sind die CBTs unabhängig vom Internet und jederzeit abrufbar. Für beide Formen gilt, dass eine Betreuung durch eine weitere Instanz nicht notwendig ist. Auf Grund dieser Einsparungen eignet sich dieses Szenario besonders für sehr große Teilnehmerzahlen.

*Blended Learning*

Bei der Diskussion um die Potentiale der neuen Medien wird häufig die Frage diskutiert, ob die neuen Medien den traditionellen Unterricht in der Schule oder Universität ablösen können. Die Forschungsbefunde zeigen eindeutig, dass das Lernen mit digitalen Medien große Potentiale bereitstellt, um bestimmte Lernwege zu unterstützen, aber nicht grundlegend besser ist als andere Lehr- und Lernformen. Vielmehr kann eine Einigung gefunden werden, dass gerade das mediengestützte Lernen in Kombination mit Elementen des traditionellen Unterrichts in einem Lernarrangements zusammenwirken. Auf diese Kombination von mediengestütztem Lernen mit Präsenzunterricht verweist das Szenario Blended Learning. Wörtlich übersetzt bedeutet Blended Learning „gemischtes Lernen“.
Die zuvor genannten Lernprogramme weisen eine recht hohe Abbruchquote auf, die durch die Ein-bindung der Lernenden in eine Gruppe oder eine betreuende Instanz reduziert werden kann. 
Bei diesem Szenario fehlt jedoch ein didaktisches Konzept, welches erläutert, wie die einzelnen Elemente didaktisch aufbereitet und kombiniert werden können.

*Videokonferenzen*

Ein weiteres E-Learning-Szenario beruht auf synchroner Kommunikation, also auf den zeitgleich anwesenden und aufeinander Bezug nehmenden Beteiligten in einem Lernarrangement. Die Lehrenden und Lernenden befinden sich an verschiedenen Orten, aber zeitgleich und können über Video- und/oder Audiokonferenzen miteinander kommunizieren.
So können weltweit MitarbeiterInnen zu einem Schulungstreffen zusammenkommen, StudentInnen können von zu Hause oder einem anderen Hörsaal der Vorlesung folgen und SchülerInnen können nach der Schule zu einem Onlinetreffen mit Mitschülern kommen. 
Eine auf diesem Prinzip beruhende Anwendung ist das virtuelle Klassenzimmer. Es werden bestimmte Funktionen zur Verfügung gestellt, die es den Teilnehmern ermöglicht, ggf. in Gruppen, zu kommunizieren, gemeinsame an Dokumenten oder Projekten zu arbeiten (z.B. per Google Docs) oder einer Präsentation zu folgen und darüber zu diskutieren.
Wenn synchrone Kommunikation für das Lehrziel erforderlich ist, dann können Videokonferenzen eine didaktisch sinnvolle Alternative zum traditionellen Unterricht darstellen (z.B. per Adobe Connect). Allerdings sollten sie nur als Zusatzelement eingesetzt werden und nicht als vollständigen Ersatz für den eigentlichen Unterricht.

*Online-Lehrgänge*

Bei Online-Lehrgängen handelt es sich um betreute Lernangebote, die meist über das Internet ablaufen. Zusätzlich gibt es Präsenztermine, wie z.B. bei Fernstudiengängen. Ein wesentliches Merkmal ist, dass die Materialien und Inhalte nicht alle gleichzeitig zur Verfügung gestellt werden, sondern didaktisch sinnvoll getaktet sind. Obwohl die Teilnehmer sich selbst organisieren und selbstständig lernen müssen, steht eine betreuende Instanz zur Verfügung und hilft bei technischen, organisatorischen oder fachli-chen Fragen weiter. Lehrgänge wie diese dauern einige Wochen bis Monate.
Ein Nachteil der Online-Lehrgänge ist, dass die Lerninhalte nur in einem ganzen Paket nach und nach zur Verfügung gestellt werden. Interessieren sich die Lernenden für eine bestimmte Lerneinheit, müssen sie darauf warten, bis diese zur Verfügung gestellt werden (wie es z.B. bei MOOCs meist der Fall ist).

*Lernmodule*

Diesem Problem können kleinere Lernmodule Abhilfe schaffen. Unterteilt man die Lerninhalte in kleinere Module, die in sich abgeschlossen sind, dann können die Lernenden sich diese bei Bedarf aufrufen. Des Weiteren müssen Eingangsvoraussetzungen zur Bearbeitung eines Lernmoduls und die Lernergebnisse, die am Ende vorhanden sein sollten, feststehen und zugänglich sein.

*Simulationen und Spiele*

Bei der Vermittlung von komplexen technischen (Flugzeug, Zug), ökonomischen (Unternehmensentwicklung), ökologischen (Klimaentwicklung) oder sozialen (demographischer Wandel, Stadtplanung) Systemen können Simulationen hilfreich sein, um den Umgang damit zu erlernen (z.B. das Spiel Eco-policy). Dafür müssen die Konstruktionspläne und Funktionen bekannt sein. Das bekannteste Beispiel sind Flugsimulationen, mit denen auch Piloten lernen, ein Flugzeug zu fliegen, indem Flugabläufe realistisch nachgestellt werden können. 
Weiterhin wird über den Einsatz von Spielen zu Lernzwecken diskutiert. Wird Lernen mit Spielen verknüpft, wird von sog. Lernspielen gesprochen, die mit didaktischer Intention entwickelt wurden. Inwiefern gelerntes Wissen durch Spiele auch in anderen Bereichen Anwendung finden kann, bleibt umstritten.
Z.B. könnte der Einsatz von Minecraft im Chemieunterricht eine sinnvolle Ergänzung sein (vgl. hancl.de).

*Communities*

Das Internet ist zu einem sozialen Raum geworden, in dem die Menschen sich nicht nur informieren und recherchieren, sondern auch in einen Austausch treten können. Auf Plattformen können sich Nutzer mit gleichen Interessen und Fragen finden und eine Community gründen.
Das bekannteste Beispiel ist die Enzyklopädie Wikipedia, die durch private Nutzer weiterentwickelt wird. So kann es zu einem zwanglosen Austausch und Teilen von Informationen kommen.

*Lernplattformen*

Als Lernplattform (Learning Management Systems LMS) werden Systeme bezeichnet, deren Aufgaben die Planung, Anmeldung, Bereitstellung von Seminarun-terlagen und Vorlesungsfolien, Lehrfilmen, Erfolgs-messung, Prüfungsorganisation, Terminverwaltung u.v.m. umfasst.
E-Learning-Plattformen bieten noch zusätzliche Foren, Chatrooms, FAQs (häufig gestellte Fragen), Lehrfilme und Gruppen an, damit sich die Lernen-den austauschen können. An der Universität Göt-tingen ist Stud.IP oder ILIAS ein solches LMS. Die Abkürzung Stud.IP steht für Studienbegleitender Internetsupport von Präsenzlehre. LMS’ wie diese werden von freien und universitären Entwicklern getragen und weiterentwickelt. Andere alternative Lernplattformen sind CLIX, Moodle, OLAT oder Chamilo.

### E-Learning Werkzeuge und Methoden
Für die Arbeit im E-Learning Bereich gibt es unzählige Werkzeuge und Methoden, die von einer simp-len Bedienung ohne großes technisches Vorwissen bis hin zu komplexen technischen Softwarepro-grammen reichen.
Als Werkzeuge werden Tools bezeichnet, die das Lernen und Lehren am Computer und im Internet ermöglichen und vereinfachen. Zum Beispiel Kom-munikationstools wie der Chat, das virtuelle Klas-senzimmer, Foren, Wikis, Weblogs oder Podcasts. Weiergibt gibt es Tools, die benötigt werden, um Bildschirminhalte oder Vorlesungen aufzuzeichnen, audiovisuelle Inhalte bereitzustellen, Konferenzen durchzuführen u.v.m..

### Warum E-Learning in der Lehre einsetzen?
Der Einsatz von E-Learning an den Hochschulen entwickelt sich, sodass immer mehr der Einbezug digitaler Lehr- und Lernformen in die Hochschul-strukturen im Vordergrund steht und für die Qualitätssicherung entscheidend ist. Dies bestätigt eine Studie des HIS-Instituts für Hochschulentwicklung im Auftrag des Hochschulforums Digitalisierung. Die Bedeutung der Digitalisierung wurde erkannt. Dies zeigt sich darin, dass 73 % der Hochschulen ein Konzept verfolgen, das beinhaltet die Lehre mit digitalen Elementen anzureichern. Gut ein Drittel der Hochschulen verfolgen sogar einen Blended-Learning-Ansatz und wollen Präsenzveranstaltungen mit E-Learning-Angeboten kombinieren. 
Digitale Werkzeuge können in stärkerem Maße individuelle Lehr- und Lernumgebungen schaffen, es fehlen jedoch noch ausgereifte didaktische Konzepte und Evaluationen.

#### Beispiele 
- Erklärvideo zu E-Learning: https://www.bpb.de/lernen/digitale-bildung/werkstatt/211328/glossar-e-learning

- E-Learning in der Hochschullehre: http://www.bildungsserver.de/Einsatz-digitaler-Medien-in-der-Hochschullehre-E-Learning.-Virtuelle-Lehre.-Mediendidaktik-1208.html

- E-Teaching-Portal: www.e-teaching.org vom Institut für Wissensmedien (IWM) inhaltlich strukturiert und von Firmen und staatlichen Einrichtungen finanziert)

- Was versteht man unter Web 2.0?  Damit ist der Wandel des Internet vom „Read Web“ zum „Read-Write Web“ gemeint: mit Wikis, Weblogs und anderen Werkzeugen wird es Nutzern ermöglicht, Inhalte ins Internet einzustel-len und der Öffentlichkeit zugänglich zu machen und dies ohne spezielle technische Kenntnisse.

#### Quellen
- Ballis, Anja & Fetscher, Doris (2009). E-Learning in der Hoch-schule. Diskurse, Didaktik, Dimensionen. München: Kopaed Verlag.

- Häfele, Hartmut & Maier-Häfele, Kornelia (2012). 101 e-Learning Seminarmethoden. Methoden und Strategien für die Online- und Blended-Learning-Seminarpraxis. ManagerSemi-nare Verlags GmbH.

- Kerres, Michael (2013). Mediendidaktik. Konzeption und Entwicklung mediengestützter Lernangebote.4. Aufl. München: Oldenbourg Verlag.

- Moritz, Werner (2008). Blended-Learning. Entwicklung, Gestal-tung, Betreuung und Evaluation von E-Learningunterstütztem Unterricht. Norderstedt: Books on Demand GmbH.
https://hochschulforumdigitalisierung.de



